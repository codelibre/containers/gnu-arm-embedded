FROM ubuntu:20.04
MAINTAINER rleigh@codelibre.net

RUN apt update \
  && DEBIAN_FRONTEND=noninteractive apt install -y -qq --no-install-recommends \
    build-essential \
    cmake \
    curl \
    doxygen \
    git \
    jq \
    man \
    locales \
    tzdata \
    graphviz \
    zip \
    unzip \
    qemu-system-arm \
    qemu-user \
    openocd \
    ninja-build \
    python3 \
    python3-pip \
    python3-regex \
    pylint \
  && locale-gen en_US.UTF-8

RUN pip install gcovr

RUN curl -Lo cmake.tar.gz https://github.com/Kitware/CMake/releases/download/v3.26.4/cmake-3.26.4-linux-x86_64.tar.gz \
    && echo "ba1e0dcc710e2f92be6263f9617510b3660fa9dc409ad2fb8190299563f952a0 *cmake.tar.gz" | sha256sum --check - \
    && cd /usr/local \
    && tar --strip-components=1 -xvf /cmake.tar.gz \
    && rm /cmake.tar.gz

RUN curl -Lo toolchain.tar.gz "https://developer.arm.com/-/media/Files/downloads/gnu/12.2.mpacbti-rel1/binrel/arm-gnu-toolchain-12.2.mpacbti-rel1-x86_64-arm-none-eabi.tar.xz?rev=71e595a1f2b6457bab9242bc4a40db90&hash=37B0C59767BAE297AEB8967E7C54705BAE9A4B95" \
    && echo "17455a06c816031cc2c66243c117cba48463cd6a3a3fdfac7275b4e9c40eb314 *toolchain.tar.gz" | sha256sum --check - \
    && cd /usr/local \
    && tar --strip-components=1 -xvf /toolchain.tar.gz \
    && rm /toolchain.tar.gz

ENV LC_ALL=en_US.UTF-8
ENV LANG=en_US.UTF-8
